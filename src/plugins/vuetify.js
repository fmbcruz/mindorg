import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '../assets/css/all.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'fal',
  },

});
